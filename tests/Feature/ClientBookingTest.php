<?php

namespace Tests\Feature;

use App\Models\Booking;
use App\Models\Client;
use DateTimeInterface;
use Generator;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\AbstractDatabaseTestCase;

class ClientBookingTest extends AbstractDatabaseTestCase
{
    /**
     * Test client bookings list.
     */
    public function test_returns_a_list_of_client_bookings(): void
    {
        $client = Booking::first()->client;
        $response = $this->getJson("/api/clients/$client->id/bookings");

        $response->assertOk();
        $response->assertJson(fn(AssertableJson $json) => $json->has(Booking::whereBelongsTo($client)->count())
            ->each(fn(AssertableJson $json) => $json->whereAllType([
                'id' => 'integer',
                'price' => ['double', 'integer'],
                'check_in_date' => 'string',
                'check_out_date' => 'string',
                'client_id' => 'integer',
                'created_at' => 'string',
                'updated_at' => 'string',
            ]))
        );
    }

    /**
     * Test creating a client's booking successfully.
     *
     * @dataProvider bookingsDataProvider
     *
     * @param float $price
     * @param DateTimeInterface $checkInDate
     * @param DateTimeInterface $checkOutDate
     */
    public function test_creating_a_clients_booking_successfully(float $price, DateTimeInterface $checkInDate, DateTimeInterface $checkOutDate): void
    {
        $client = Client::first();
        $nBookings = Booking::count();
        $response = $this->postJson(
            "/api/clients/$client->id/bookings",
            [
                'price' => $price,
                'check_in_date' => $checkInDate->format('c'),
                'check_out_date' => $checkOutDate->format('c'),
            ]
        );

        $response->assertCreated();
        $response->assertJson(fn(AssertableJson $json) => $json->where('id', $nBookings + 1)
            ->where('price', $price)
            ->where('check_in_date', $checkInDate->format('c'))
            ->where('check_out_date', $checkOutDate->format('c'))
            ->where('client_id', $client->id)
            ->etc()
        );
    }

    /**
     * Bookings data provider.
     *
     * @return Generator
     */
    public function bookingsDataProvider(): Generator
    {
        for ($i = 0; $i < 1; $i++) {
            yield [
                $this->faker('en')->randomFloat(2, 10, 100),
                $this->faker('en')->dateTimeBetween(now()->addDay(), now()->addMonth()),
                $this->faker('en')->dateTimeBetween(now()->addMonth(), now()->addMonths(2)),
            ];
        }
    }
}
