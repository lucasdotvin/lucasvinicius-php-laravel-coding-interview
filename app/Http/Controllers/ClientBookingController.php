<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClientBookingController extends Controller
{
    private const DOG_AGE_DISCOUNT_THRESHOLD = 10;

    /**
     * Get client's bookings.
     *
     * @param Client $client
     *
     * @return Collection
     */
    public function index(Client $client): Collection
    {
        return $client->bookings;
    }

    /**
     * Get a booking.
     *
     * @param Booking $booking
     *
     * @return Booking
     */
    public function show(Booking $booking): Booking
    {
        return $booking;
    }

    /**
     * Create a booking.
     *
     * @param Request $request
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function store(Request $request, Client $client): JsonResponse
    {
        $bookingData = $this->getValidatedData($request);

        $dogAgeSum = $client->dogs()->sum('age');
        $dogsCount = $client->dogs()->count();
        $averageDogAge = $dogAgeSum / $dogsCount;

        if ($dogsCount and $averageDogAge < self::DOG_AGE_DISCOUNT_THRESHOLD) {
            $bookingData['price'] = $bookingData['price'] * 0.9;
        }

        $booking = new Booking($bookingData);
        $client->bookings()->save($booking);

        return response()->json($booking, 201);
    }

    /**
     * Get validated data.
     *
     * @param Request $request
     *
     * @return array
     */
    private function getValidatedData(Request $request): array
    {
        return $request->validate([
            'price' => [
                'required',
                'numeric',
                'gt:0'
            ],
            'check_in_date' => [
                'required',
                'date',
                'after:now',
            ],
            'check_out_date' => [
                'required',
                'date',
                'after:check_in_date',
            ],
        ]);
    }
}
